﻿using System;
class Example
{

    static void Main()
    {
        int t, i;
        int[,] table = new int[10, 20];

        for (t=0;t<10;t++)
        {
            for (i=0;i<20;i++)
            {
                table[t, i] = (t * 4) + i + 1;
                Console.WriteLine(table[t, i] + " ");
            }
        }
        Console.WriteLine();
    }

}