﻿//average of sequence
using System;

class Example
{
    static void Main()
    {

        Random rnd = new Random();

        int[] nums = new int[10];
        int i, sum=0,avg=0;
        for(i = 0; i < 10; i++)
        {
            nums[i] = rnd.Next(1,50);
        }

        for (i = 0; i < 10; i++)
        {
            sum += nums[i];
        }

        avg = sum / 10;
        
        Console.WriteLine("The average number is {0}",avg);
    }
}
