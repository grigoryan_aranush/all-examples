﻿using System;
class Example
{
    static void Main()
    {
        int i;
        int[] sample = new int[10];
        for (i = 0; i < 10; i++)
            sample[i] = i * i;

        for (i = 0; i < 10; i++)
            Console.WriteLine("the {0}th element is {1}",i,sample[i]);

    }
}