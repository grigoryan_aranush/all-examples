﻿using System;
class Example
{
    static void Main() {

        int num,nextDigit, numdigits;
        int[] n = new int[20];
        
        string[] Digits =
                        {
                             "zero", "one", "two",
                             "three", "four", "five",
                             "six", "seven", "eight",
                             "nine"
                        };


        num = 1908;
        nextDigit = 0;
        numdigits = 0;

        Console.WriteLine("Number is {0} ",num);


        do
        {
            nextDigit = num % 10;
            n[numdigits] = nextDigit;
            num = num / 10;
            numdigits++;
        } while (num>0);

        numdigits--;
        Console.Write("number in words: ");
        for(; numdigits >= 0; numdigits--)
        {
            Console.Write(Digits[n[numdigits]]+" ");
        }
           
    }
}
