﻿
// Swap two values.
using System;
class ValueSwap
{
    public void Swap(ref int a, ref int b)
    {
        int t;
        t = a;
        a = b;
        b = t;

    }
}
class DemoSwap
{
    static void Main()
    {

        ValueSwap ob = new ValueSwap();
        int a = 10, b = 15;
        Console.WriteLine("before swapping  a was {0}, b was {1}", a, b);
        ob.Swap(ref a, ref b);
        Console.WriteLine("after swapping  a is {0}, b is {1}", a, b);

    }
}