﻿//Determine if x and y have a common divisor.
using System;

class Num
{
    public bool HasComFactor(int x,int y,out int least, out int greatest)
    {
        int i;
        int max = (x > y) ? x : y;
        bool first;
        first = true;
        least = 1;
        greatest = 1;

        for (i = 2; i <= max / 2; i++)
        {
            if(((x % i == 0) & (y % i == 0)))
            {
                if (first)
                {
                    least = i;
                    first = false;
                }
                greatest = i;
                
            }
        }

        if (least==1)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
}
class Demo
{
    static void Main()
    {
        Num ob = new Num();
        int lcf, gcf;
        if(ob.HasComFactor(10,500, out lcf, out gcf))
        {

            Console.WriteLine("Lcf of 10 and 500 is " + lcf);
            Console.WriteLine("Gcf of 10 and 500 is " + gcf);
        }
        else
            Console.WriteLine("No common factor for 10 and 500.");

        if (ob.HasComFactor(35, 51, out lcf, out gcf))
        {

            Console.WriteLine("Lcf of 35 and 51 is " + lcf);
            Console.WriteLine("Gcf of 35 and 51 is " + gcf);
        }
        else
            Console.WriteLine("No common factor for 35 and 51.");



    }
}