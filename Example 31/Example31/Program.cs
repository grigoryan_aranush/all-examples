﻿using System;
class Building
{
    public int Area;
    public int Floors;
    public int Occupants;

    public int AreaPerPerson()
    {
        return Area / Occupants;
    }

    public int MaxOccupant(int minArea)
    {
        return Area / minArea;
    }
}
class BuildingDemo
{

    static void Main()
    {
        Building house = new Building();
        Building office = new Building();

        house.Occupants = 4;
        house.Area = 50;
        house.Floors = 1;

        office.Occupants = 50;
        office.Area = 2500;
        office.Floors = 2;

        Console.WriteLine("house areaPP is " + house.AreaPerPerson()+"the maximum occupant number is " + house.MaxOccupant(10));
        Console.WriteLine("office areaPP is " + office.AreaPerPerson()+"the maximum occupant number is " + office.MaxOccupant(5));




    }

}