﻿//Stack 

using System;


class Stack
{

    char[] stack;
    int top;

    public Stack(int size)
    {
        stack = new char[size];
        top = 0;

    }
    // Push characters onto the stack.

    public void Push( char ch)
    {

        if (top == stack.Length)
        {
            Console.WriteLine("stack is full");

        }
        else
        {
            stack[top] = ch;
            top++;
        }

    }
    // Pop a character from the stack.
    public char Pop()
    {
        if (top == 0)
        {
            Console.WriteLine("String is empty");
            return (char)0;
        }
        top--;
        return stack[top];

    }
    // Return true if the stack is full.

    public bool IsFull()
    {
        
        return top == stack.Length;

    }
    // Return true if the stack is empty.

    public bool IsEmpty()
    {

        return top == 0;

    }
    // Return total capacity of the stack.

    public int Capacity()
    {
        return stack.Length;
    
    }
    // Return number of objects currently on the stack.

    public int GetNum()
    {
        return top;
    }
}


class StackDemo
{
    static void Main()
    {
        Stack stk1 = new Stack(10);
        Stack stk2 = new Stack(10);
        int i;

        // Put some characters into stk1.

        for (i = 0; !stk1.IsFull(); i++)
        {
              
            stk1.Push((char)('A' + i));
        }

        if (stk1.IsFull())
            Console.WriteLine("steck 1 is full");

        // Display the contents of stk1.
        while (!stk1.IsEmpty())
        {
            Console.Write(stk1.Pop() + " ");
        }

        Console.WriteLine("Stk1 is empty");

        // Put more characters into stk1.
        for (i = 0; !stk1.IsFull(); i++)
        {

            stk1.Push((char)('A' + i));
        }

        // Now, pop from stk1 and push the element in stk2.

        while (!stk1.IsEmpty())
        {
            stk2.Push(stk1.Pop());
        }
        // Display the contents of stk2.
        Console.Write("Contents of stk2: ");
        while (!stk2.IsEmpty())
        {
            Console.Write(stk2.Pop() + " ");
        }

        Console.WriteLine("stk2 is empty");

    }
}