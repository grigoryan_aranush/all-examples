﻿using System;
class Example
{
    //Array overrun
    static void Main()
    {
        int[] sample = new int[10];
        int i;
        for (i = 0; i < 10; i++)
        {
            sample[i] = i;
        }
        for (i = 0; i < 100; i++)
        {
            Console.WriteLine("The {0}th element is {1}", i, sample[i]);
        }
    }
}