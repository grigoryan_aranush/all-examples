﻿using System;
class Example
{

    static void Main()
    {
        int i, j;
        int[] nums1 = new int[10];
        int[] nums2 = new int[10];
        for (i = 0; i < nums1.Length; i++) nums1[i] = i*i;

        Console.Write("the origin nums1 numbers are ");
        for(i = 0; i < nums1.Length; i++)
        {
            Console.Write(nums1[i]+ " ");
        }
        Console.WriteLine();

        if (nums2.Length <= nums1.Length)
        {
            for(i= nums1.Length-1, j=0;(i>0 && j < nums2.Length); i--, j++)
            {
                nums2[j] = nums1[i];
            }
        }


        Console.Write("Reversed array is: ");
        for (i = 0; i < nums2.Length; i++)
            Console.Write(nums2[i]+" ");

        Console.WriteLine();

    }
}