﻿using System;
class Example
{
    static void Main()
    {
        int[,] sqrs =
        {
            {1,1},
            {2,2},
            {5,5},
            {5,5},
            {5,5},
            {5,5},
            {5,5},
            {5,5},
            {5,5},
            {5,5},
        };
        int i, j;
        for (i = 0; i < 10; i++)
        {
            for (j = 0; j < 2; j++)
            {
                Console.WriteLine(sqrs[i, j] + " ");
            }
        }
        Console.WriteLine();
    }
}